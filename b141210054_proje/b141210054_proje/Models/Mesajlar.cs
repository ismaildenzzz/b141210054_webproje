﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b141210054_proje.Models
{
    public class Mesajlar
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Eposta { get; set; }
        public string Tel { get; set; }
        public string Konu { get; set; }
        public string Mesaj { get; set; }
        public string Tarih { get; set; }
    }
}