﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b141210054_proje.Models
{
    public class Yorumlar
    {
        public int Id { get; set; }
        public int Kim { get; set; }
        public int Kime { get; set; }
        public System.DateTime Tarih { get; set; }
        public string Yorum { get; set; }
        public int Onay { get; set; }
        public virtual Kullanicilar Kullanici { get; set; }
    }
}