﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b141210054_proje.Models
{
    public class Icerikler
    {
        public int Id { get; set; }
        public string Baslik { get; set; }
        public string Basliken { get; set; }
        public string SeoDesc { get; set; }
        public string SeoKey { get; set; }
        public string Resim { get; set; }
        public string Konu { get; set; }
        public string Icerik { get; set; }
        public string Tur { get; set; }
        public DateTime? Tarih { get; set; }
        public string Ozet { get; set; }
        public string DersAdi { get; set; }
        public int? AdminK { get; set; }
        public int Onay { get; set; }
        public virtual Adminler Admin { get; set; }
    }
}