﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b141210054_proje.Models
{
    public class Kullanicilar
    {
        public int Id { get; set; }
        public string KullaniciAd { get; set; }
        public string KullaniciSoyad { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Hakkinda { get; set; }
        public string KullaniciResim { get; set; }
        public Nullable<int> Onay { get; set; }
        public virtual ICollection<Yorumlar> Yorum { get; set; }
    }
}