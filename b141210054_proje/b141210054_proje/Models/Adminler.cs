﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace b141210054_proje.Models
{
    public class Adminler
    {
        public int Id { get; set; }
        public string AdminAd { get; set; }
        public string AdminSoyad { get; set; }
        public string A_Username { get; set; }
        public string A_Password { get; set; }
        public string AdminImage { get; set; }
        public string AdminTel { get; set; }
        public string AdminMail { get; set; }
        public virtual ICollection<Icerikler> Icerik { get; set; } //Navigation Properties
        //İçerik tablosunda bu admine ilişkin birden fazla kayıt bulunabilir
    }
}