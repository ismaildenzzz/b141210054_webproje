﻿using b141210054_proje.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace b141210054_proje.DAL
{
    public class projesEntities : DbContext
    {
        public projesEntities() : base("proje")
        {
        }
 
        public DbSet<Icerikler> Icerik { get; set; }
        public DbSet<Yorumlar> Yorum { get; set; }
        public DbSet<Adminler> Admin { get; set; }
        public DbSet<Kullanicilar> Kullanici { get; set; }
        public DbSet<Hakkimizda> Hakkimiz { get; set; }
        public DbSet<Mesajlar> Mesaj { get; set; }
        public DbSet<Register> Regist { get; set; }
        public DbSet<Login> Login { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}