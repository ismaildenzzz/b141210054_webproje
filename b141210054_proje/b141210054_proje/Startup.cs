﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(b141210054_proje.Startup))]
namespace b141210054_proje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
