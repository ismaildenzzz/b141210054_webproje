﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b141210054_proje.Utils
{
    public class BaseController : System.Web.Mvc.Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.RouteData.Values["Controller"].ToString();

            if(controllerName=="Admin")
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() != "1")
                {
                    filterContext.Result = new RedirectResult("/Login/Index");
                    return;
                }
                base.OnActionExecuting(filterContext);
            }
            
        }
    }
}