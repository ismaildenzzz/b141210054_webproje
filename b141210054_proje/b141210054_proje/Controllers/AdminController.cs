﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b141210054_proje.Models;
using b141210054_proje.Utils;
using b141210054_proje.DAL;
using System.IO;
using System.Data.Entity;
using System.Net;

namespace b141210054_proje.Controllers
{
    public class AdminController : BaseController
    {
        // GET: Admin
        projesEntities db2 = new projesEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProfilDuzenle(int? Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adminler admin = db2.Admin.Find(Id);
            if (admin == null)
            {
                return HttpNotFound();
            }
            return View(admin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProfilDuzenle([Bind(Include = "Id,AdminAd,AdminSoyad,A_Username,A_Password,AdminImage,AdminTel,AdminMail")] Adminler admin)
        {
            if (ModelState.IsValid)
            {
                db2.Entry(admin).State = EntityState.Modified;
                db2.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(admin);
        }

        private bool isValidContentType(string contentType)
        {
            return contentType.Equals("image/png") || contentType.Equals("image/jpg") || contentType.Equals("image/gif") || contentType.Equals("image/jpeg");
        }

        public ActionResult IcerikEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult IcerikEkle(FormCollection form,HttpPostedFileBase resim)
        {
            if(!isValidContentType(resim.ContentType))
            {
                ViewBag.Error = "Sadece jpg , png , jpeg ,gif türünde resim ekleyebilirsiniz.";
                
            }
            else
            {
                Icerikler model = new Icerikler();
                var resimAdi = Path.GetFileName(resim.FileName);
                var uzanti = Path.Combine(Server.MapPath("~/assets/uploads"),resimAdi);
                resim.SaveAs(uzanti);
                model.Baslik = form["baslik"];
                model.SeoDesc = form["desc"];
                model.SeoKey = form["keys"];
                model.Konu = form["konu"];
                model.Tur = form["tur"];
                model.Resim = "/assets/uploads/"+resimAdi;
                model.Ozet = form["ozet"];
                model.Icerik = form["yazi"];
                model.DersAdi = form["ders"];
                model.Tarih = DateTime.Now;
                model.Onay = 1;
                db2.Icerik.Add(model);
                db2.SaveChanges();
                return RedirectToAction("IcerikListele", "Admin");
            }
            return View("IcerikEkle");

        }

        public ActionResult Hakkimizda()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Hakkimizda(FormCollection form)
        {
            Hakkimizda model = new Hakkimizda();
            model.Baslik = form["baslik"];
            model.Icerik = form["yazi"];
            model.Tarih = DateTime.Now;
            db2.Hakkimiz.Add(model);
            db2.SaveChanges();
            return RedirectToAction("Index", "Admin");
        }
        
        public ActionResult IcerikListele()
        {
            AdminDTO obj = new AdminDTO();
            obj.icerik = db2.Icerik.OrderByDescending(x => x.Tarih).ToList();
            return View(obj);
        }

        public ActionResult IcerikSil(int Id = 0)
        {
            Icerikler icerik = db2.Icerik.Find(Id);
            if(icerik==null)
            {
                return HttpNotFound();
            }
            return View(icerik);
        }

        [HttpPost,ActionName("IcerikSil")]
        public ActionResult IcerikSilindi(int Id)
        {
            Icerikler icerik = db2.Icerik.Find(Id);
            db2.Icerik.Remove(icerik);
            db2.SaveChanges();
            return RedirectToAction("IcerikListele");
        }

        public ActionResult IcerikDuzenle(int Id = 0)
        {
            Icerikler icerik = db2.Icerik.Find(Id);
            if(icerik == null)
            {
                return HttpNotFound();
            }
            return View(icerik);
        }

        [HttpPost]
        public ActionResult IcerikDuzenle(int Id,FormCollection form, HttpPostedFileBase resim)
        {
            
            Icerikler model = db2.Icerik.Find(Id);
            var resimAdi = Path.GetFileName(resim.FileName);
            var uzanti = Path.Combine(Server.MapPath("~/assets/uploads"), resimAdi);
            resim.SaveAs(uzanti);
            model.Baslik = form["baslik"];
            model.SeoDesc = form["desc"];
            model.SeoKey = form["keys"];
            model.Konu = form["konu"];
            model.Tur = form["tur"];
            model.Resim = "/assets/uploads/" + resimAdi;
            model.Ozet = form["ozet"];
            model.Icerik = form["yazi"];
            model.DersAdi = form["ders"];
            model.Tarih = DateTime.Now;
            model.Onay = 1;
            db2.Icerik.Add(model);
            db2.SaveChanges();
           
            return RedirectToAction("IcerikListele", "Admin");
        }

        public ActionResult YorumListele()
        {
            return View(db2.Yorum.ToList());
        }

        public ActionResult YorumDetay(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yorumlar yorumlar = db2.Yorum.Find(id);
            if (yorumlar == null)
            {
                return HttpNotFound();
            }
            return View(yorumlar);
        }

        // GET: Yorum/Edit/5
        public ActionResult YorumDuzenle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yorumlar yorumlar = db2.Yorum.Find(id);
            if (yorumlar == null)
            {
                return HttpNotFound();
            }
            return View(yorumlar);
        }

        // POST: Yorum/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult YorumDuzenle([Bind(Include = "Id,Kim,Kime,Tarih,Yorum,Onay")] Yorumlar yorumlar)
        {
            if (ModelState.IsValid)
            {
                db2.Entry(yorumlar).State = EntityState.Modified;
                db2.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(yorumlar);
        }

        // GET: Yorum/Delete/5
        public ActionResult YorumSil(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yorumlar yorumlar = db2.Yorum.Find(id);
            if (yorumlar == null)
            {
                return HttpNotFound();
            }
            return View(yorumlar);
        }

        // POST: Yorum/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult YorumSilindi(int id)
        {
            Yorumlar yorumlar = db2.Yorum.Find(id);
            db2.Yorum.Remove(yorumlar);
            db2.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db2.Dispose();
            }
            base.Dispose(disposing);
        }


    }

    public class AdminDTO   
    {
        public List<Icerikler> icerik { get; set; }
        public List<Yorumlar> yorumlar { get; set; }
        public List<Adminler> adminler { get; set; }
        public List<Mesajlar> mesajlar { get; set; }
        public List<Hakkimizda> hakkimizda { get; set; }
    }

}


/*
            model.Baslik = form["baslik"];
            model.SeoDesc = form["desc"];
            model.SeoKey = form["keys"];
            model.Konu = form["konu"];
            model.Tur = form["tur"];
            model.Ozet = form["ozet"];
            model.Icerik = form["yazi"];
            model.DersAdi = form["ders"];
            model.Onay = 1;
            model.Tarih = DateTime.Now;
            db2.Icerikler.Add(model);
            db2.SaveChanges();
            */
