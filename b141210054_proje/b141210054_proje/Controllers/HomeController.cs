﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b141210054_proje.DAL;
using b141210054_proje.Models;
using System.Net;

namespace b141210054_proje.Controllers
{
    public class HomeController : Controller
    {

        projesEntities db = new projesEntities();
           
        public ActionResult Index()
        {
             AnasayfaDTO obj = new AnasayfaDTO();
             obj.icerik = db.Icerik.OrderByDescending(x => x.Tarih).ToList();
             return View(obj);
        }

        public ActionResult Hakkimizda()
        {
            AnasayfaDTO obj = new AnasayfaDTO();
            obj.hakkimizda = db.Hakkimiz.OrderByDescending(x => x.Tarih).Take(1).ToList();
            return View(obj);
        }

        public ActionResult Dersler()
        {
            AnasayfaDTO obj = new AnasayfaDTO();
            obj.icerik = db.Icerik.Where(x => x.Tur=="Dersler").OrderByDescending(x => x.Tarih).ToList();
            return View(obj);
        }

        public ActionResult Gundem()
        {
            AnasayfaDTO obj = new AnasayfaDTO();
            obj.icerik = db.Icerik.Where(x => x.Tur == "Gundem").OrderByDescending(x => x.Tarih).ToList();
            return View(obj);
        }
                
        public ActionResult IcerikDetay(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Icerikler icerikler = db.Icerik.Find(id);
            if (icerikler == null)
            {
                return HttpNotFound();
            }
            return View(icerikler);
        }

        public ActionResult Dil_Tr()
        {
            if(Session["Dil"]!=null)
                Session.Remove("Dil");

            Session["Dil"] = "tr";
            return RedirectToAction("Index");
        }
        public ActionResult Dil_En()
        {
            if (Session["Dil"] != null)
                Session.Remove("Dil");

            Session["Dil"] = "en";
            return RedirectToAction("Index");
        }

    }
    public class AnasayfaDTO
    {
        public List<Icerikler> icerik { get; set; }
        public List<Hakkimizda> hakkimizda { get; set; }
        public List<Yorumlar> yorumlar { get; set; }
    }
}